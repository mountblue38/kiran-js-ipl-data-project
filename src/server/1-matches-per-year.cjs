const Papa = require("papaparse");
const fs = require("fs");

const file = fs.createReadStream("../data/matches.csv");
Papa.parse(file, {
    header: true,
    complete: function (results, file) {
        problem1(results);
    },
});

function problem1(returedArrayOfObjects) {
    // let ans = {};
    // returedArrayOfObjects.data.map(function (objectData) {
    //     if (objectData.season in ans) {
    //         ans[objectData.season] += 1;
    //     } else {
    //         ans[objectData.season] = 1;
    //     }
    // });

    let ans = returedArrayOfObjects.data.reduce(function (acc, curr) {
        if (curr.season in acc) {
            acc[curr.season] += 1;
        } else {
            acc[curr.season] = 1;
        }
        return acc;
    }, {});

    try {
        fs.writeFileSync(
            "../public/output/1-matches-per-year.json",
            JSON.stringify(ans)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
