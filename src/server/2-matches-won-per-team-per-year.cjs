const Papa = require("papaparse");
const fs = require("fs");

const file = fs.createReadStream("../data/matches.csv");
Papa.parse(file, {
    header: true,
    complete: function (results, file) {
        problem2(results);
    },
});

function problem2(returedArrayOfObjects) {
    let outputObj = {};
    returedArrayOfObjects.data.map(function (objectData) {
        if (!(objectData.season in outputObj)) {
            outputObj[objectData.season] = {};
        }
        if (objectData.winner in outputObj[objectData.season]) {
            outputObj[objectData.season][objectData.winner] += 1;
        } else {
            outputObj[objectData.season][objectData.winner] = 1;
        }
    });

    try {
        fs.writeFileSync(
            "../public/output/2-matches-won-per-team-per-year.json",
            JSON.stringify(outputObj)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
