const Papa = require("papaparse");
const fs = require("fs");

const deliveriesfile = fs.createReadStream("../data/deliveries.csv");
const matchesfile = fs.createReadStream("../data/matches.csv");
filesLst = [deliveriesfile, matchesfile];

const parseFiles = (files) => {
    Promise.all(
        files.map((file) => {
            return new Promise((resolve, reject) =>
                Papa.parse(file, {
                    header: true,
                    complete: resolve,
                    error: reject,
                })
            );
        })
    )
        .then((results) => {
            problem4(results);
        })
        .catch((err) => {
            console.log(err);
        });
};
parseFiles(filesLst);

function problem4(returedArrayOfObjects) {
    let ecoObj = {};
    let deliverysData = returedArrayOfObjects[0];
    let matchesData = returedArrayOfObjects[1];
    let filteredData = matchesData.data.filter((element) => {
        return element.season == "2015";
    });
    filteredData.map((matchData) => {
        deliverysData.data.map((delData) => {
            if (!(matchData.season in ecoObj)) {
                ecoObj[matchData.season] = {};
            }
            if (matchData.id == delData.match_id) {
                if (!(delData.bowler in ecoObj[matchData.season])) {
                    ecoObj[matchData.season][delData.bowler] = {bowler:"",runs:0,balls:0};
                }
                ecoObj[matchData.season][delData.bowler].runs +=Number(delData.batsman_runs);
                ecoObj[matchData.season][delData.bowler].balls +=1;
                ecoObj[matchData.season][delData.bowler].bowler = delData.bowler;
            }
            
        });
    });
    let finalArr = []
    Object.keys(ecoObj).map((year)=>{
        Object.keys(ecoObj[year]).map((player)=>{
           let economy = Number(((ecoObj[year][player].runs/ecoObj[year][player].balls)*6).toFixed(2));
           finalArr.push({"bowler":ecoObj[year][player].bowler,"economy":economy});
        })
    })
    function compare(value1, value2) {
        if (value1.economy < value2.economy) {
            return -1;
        }
        if (value1.economy > value2.economy) {
            return 1;
        }
        return 0;
    }
    try {
        fs.writeFileSync(
            "../public/output/4-top10-economical-bowler-2015.json",
            JSON.stringify(finalArr.sort(compare).slice(0,10))
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
