const Papa = require("papaparse");
const fs = require("fs");

const deliveriesfile = fs.createReadStream("../data/deliveries.csv");
const matchesfile = fs.createReadStream("../data/matches.csv");
filesLst = [deliveriesfile, matchesfile];

const parseFiles = (files) => {
    Promise.all(
        files.map((file) => {
            return new Promise((resolve, reject) =>
                Papa.parse(file, {
                    header: true,
                    complete: resolve,
                    error: reject,
                })
            );
        })
    )
        .then((results) => {
            problem7(results);
        })
        .catch((err) => {
            console.log(err);
        });
};
parseFiles(filesLst);

function problem7(returedArrayOfObjects) {
    let outputObj = {};
    let deliverysData = returedArrayOfObjects[0];
    let matchesData = returedArrayOfObjects[1];
    matchesData.data.map((matchData) => {
        deliverysData.data.map((delData) => {
            if (!(matchData.season in outputObj)) {
                outputObj[matchData.season] = {};
            }
            if (matchData.id == delData.match_id) {
                if (!(delData.batsman in outputObj[matchData.season])) {
                    outputObj[matchData.season][delData.batsman] = {runs:0,balls:0};
                }
                outputObj[matchData.season][delData.batsman].runs +=Number(delData.batsman_runs);
                outputObj[matchData.season][delData.batsman].balls +=1;
            }
            
        });
    });
    Object.keys(outputObj).map((year)=>{
        Object.keys(outputObj[year]).map(player=>{
           outputObj[year][player] = (outputObj[year][player].runs/outputObj[year][player].balls)*100;
        })
    })

    try {
        fs.writeFileSync(
            "../public/output/7-strike-rate-of-batsman-per-season.json",
            JSON.stringify(outputObj)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
