const Papa = require("papaparse");
const fs = require("fs");

const file = fs.createReadStream("../data/matches.csv");
Papa.parse(file, {
    header: true,
    complete: function (results, file) {
        problem5(results);
    },
});

function problem5(returedArrayOfObjects) {
    let outputObj = {};
    returedArrayOfObjects.data.map(function (objectData) {
        if (!(objectData.season in outputObj)) {
            outputObj[objectData.season] = {};
        }
        if (objectData.winner == objectData.toss_winner) {
            if (objectData.winner in outputObj[objectData.season]) {
                outputObj[objectData.season][objectData.winner] += 1;
            } else {
                outputObj[objectData.season][objectData.winner] = 1;
            }
        }
    });


    try {
        fs.writeFileSync(
            "../public/output/5-number-of-times-team-won-both-toss-match.json",
            JSON.stringify(outputObj)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
