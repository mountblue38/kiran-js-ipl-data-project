const Papa = require("papaparse");
const fs = require("fs");

const deliveriesfile = fs.createReadStream("../data/deliveries.csv");

Papa.parse(deliveriesfile, {
    header: true,
    complete: function (results) {
        problem9(results);
    },
});

function problem9(returedArrayOfObjects) {
    let delData = returedArrayOfObjects.data;
    let econ = {};

    function getOvers(bowler) {
        let balls = 0;
        delData.map((data) => {
            if (data.bowler == bowler && data.is_super_over == 1) {
                balls += 1;
            }
        });
        return balls / 6;
    }

    delData.map((ob) => {
        if (ob.is_super_over == 1) {
            if (!econ.hasOwnProperty(ob.bowler)) {
                econ[ob.bowler] = parseInt(ob.total_runs);
            } else {
                econ[ob.bowler] += parseInt(ob.total_runs);
            }
        }
    });
    console.log(econ)
    // for (let key in econ) {
    //     econ[key] = Math.round((econ[key] / getOvers(key)) * 100) / 100;
    // }
    // Object.keys(econ).map((key)=>{
    //     econ[key] = Math.round((econ[key] / getOvers(key)) * 100) / 100;
    //   })
    console.log(econ);
  function compare(value1, value2) {
    if (value1.economy < value2.economy) {
        return -1;
    }
    if (value1.economy > value2.economy) {
        return 1;
    }
    return 0;
}

      

    try {
        fs.writeFileSync(
            "../public/output/9-bowler-with-the-best-economy-in-super-overs.json",
            JSON.stringify(econ)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
