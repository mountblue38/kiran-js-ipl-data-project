const Papa = require("papaparse");
const fs = require("fs");

const deliveriesfile = fs.createReadStream("../data/deliveries.csv");

Papa.parse(deliveriesfile, {
    header: true,
    complete: function(results){
        problem8(results);
    }
})

function problem8(returedArrayOfObjects) {
    let dismissedData = {};
    deliverysData.map((delData)=>{
        if(!(delData.player_dismissed in dismissedData)){
            dismissedData[delData.player_dismissed] = {};
            // ecoObj[matchData.season][delData.bowler] = {bowler:"",runs:0,balls:0};
        }
        // console.log(dismissedData[delData.player_dismissed][delData.bowler])
        // if( delData.player_dismissed in dismissedData && delData.bowler == dismissedData[player_dismissed][delData.bowler]) 
        // console.log(dismissedData)
        else if(delData.player_dismissed in dismissedData && dismissedData[delData.player_dismissed].bowler == delData.bowler){
            dismissedData[delData.player_dismissed].noOfTimes +=1;
        }
    })
// console.log(dismissedData);

// let  highestDismissed = Object.keys(dismissedPlayer).map(function(key) {
//   return [key, dismissedPlayer[key]];
// }).sort(function(curr, next){ return next[1] - curr[1]; });

// console.log("dis",dismissedData)
try {
    fs.writeFileSync(
        "../public/output/8-highest-number-of-times-one-player-has-been-dismissed.json",
        JSON.stringify(dismissedData)
    );
} catch (error) {
    console.log("An error has occurred ", error);
}

}