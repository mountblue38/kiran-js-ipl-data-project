const Papa = require("papaparse");
const fs = require("fs");

const file = fs.createReadStream("../data/matches.csv");
Papa.parse(file, {
    header: true,
    complete: function (results, file) {
        problem6(results);
    },
});

function problem6(returedArrayOfObjects) {
    let resObj = {};
    let count, maxMOM;
    let outputObj = {};
    returedArrayOfObjects.data.map(function (matchData) {
        if (!(matchData.season in resObj)) {
            count = 0;
            maxMOM = "";
            resObj[matchData.season] = {};
        }
        if (matchData.player_of_match in resObj[matchData.season]) {
            if (resObj[matchData.season][matchData.player_of_match] > count) {
                count = resObj[matchData.season][matchData.player_of_match];
                maxMOM = matchData.player_of_match;
            }
            resObj[matchData.season][matchData.player_of_match] += 1;
        } else {
            resObj[matchData.season][matchData.player_of_match] = 1;
        }
        outputObj[matchData.season] = maxMOM;
    });

    try {
        fs.writeFileSync(
            "../public/output/6-most-man-of-the-match-per-season.json",
            JSON.stringify(outputObj)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
