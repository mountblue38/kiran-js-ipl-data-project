const Papa = require("papaparse");
const fs = require("fs");

const deliveriesfile = fs.createReadStream("../data/deliveries.csv");
const matchesfile = fs.createReadStream("../data/matches.csv");
filesLst = [deliveriesfile, matchesfile];

const parseFiles = (files) => {
    Promise.all(
        files.map((file) => {
            return new Promise((resolve, reject) =>
                Papa.parse(file, {
                    header: true,
                    complete: resolve,
                    error: reject,
                })
            );
        })
    )
        .then((results) => {
            problem3(results);
        })
        .catch((err) => {
            console.log(err);
        });
};
parseFiles(filesLst);

function problem3(returedArrayOfObjects) {
    let outputObj = {};
    let deliverysData = returedArrayOfObjects[0];
    let matchesData = returedArrayOfObjects[1];
    let filteredData = matchesData.data.filter((element) => {
        return element.season == "2016";
    });
    filteredData.map((matchData) => {
        deliverysData.data.map((delData) => {
            if (matchData.id == delData.match_id) {
                if (!(delData.bowling_team in outputObj)) {
                    outputObj[delData.bowling_team] = 0;
                }
                if (delData.extra_runs > 0) {
                    outputObj[delData.bowling_team] += Number(
                        delData.extra_runs
                    );
                }
            }
        });
    });

    try {
        fs.writeFileSync(
            "../public/output/3-extra-runs-conceded-per-team-in2016.json",
            JSON.stringify(outputObj)
        );
    } catch (error) {
        console.log("An error has occurred ", error);
    }
}
